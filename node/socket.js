const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
var user = [];

// app.get('/', (req, res) => {
//     // res.sendFile(__dirname + '/index.html');
// });
let count = 0;
io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('user_data', (msg) => {

        user.push(JSON.parse(msg));
        console.log(compare());
    })
    socket.on("user_rank", () => {
        io.emit('user_rank', user);
        console.log(JSON.stringify(user).toString());
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});
http.listen(5000, () => {
    console.log('Connected at 3000');
});

function compare() {

    user.sort(function (a, b) {
        // boolean false == 0; true == 1
        return a.score - b.score;
    });
    return user;
    // 順序依序會是 Mike -> Jimmy -> Judy
}
//JSON.stringify(user)